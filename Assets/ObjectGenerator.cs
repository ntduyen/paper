using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObjectGenerator : MonoBehaviour
{
	public GameObject objectPrefab;
	public int numberOfObjects = 5;
	public float spacing = 10f;
	[SerializeField] private GameObject bar;

	private string str;
	private List<int> list = new() { 1, 2, 3, 4 };

	private void Start()
	{
		str = string.Join('-', list);
		foreach (string i in str.Split('-'))
		{
			list.Add(Int32.Parse(i));
		}
		foreach(int i in list)
		{
            //Logger.Debug(i);
		}

		GenerateObjects();
		StartCoroutine(GenerateMoreObjects());
	}

	private void GenerateObjects()
	{
		for (int i = 0; i < numberOfObjects; i++)
		{
			GameObject newObject = Instantiate(objectPrefab, transform);
			newObject.SetActive(true);
			//newObject.transform.SetParent(transform);
			newObject.transform.localPosition = Vector3.one;
		}
	}
	IEnumerator GenerateMoreObjects()
	{
		yield return new WaitForSeconds(5f);
		Vector2 size = bar.GetComponent<RectTransform>().sizeDelta;
		bar.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x + 10f, size.y);
		for (int i = 0; i < 2; i++)
		{
			GameObject newObject = Instantiate(objectPrefab, transform);
			newObject.SetActive(true);
			newObject.transform.localPosition = Vector3.one;
		}
	}


}
