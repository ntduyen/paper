using UnityEngine;

namespace PaperFold2D
{
	public class SaveAndLoadLevel : MonoBehaviour
	{
		[SerializeField] private int level = 0;
		[SerializeField] private Level levels;
		[SerializeField] private GameObject parentObject;
		[SerializeField] private AudioSource backgroundSound;
		[SerializeField] private GameLoadingView loadingView;
		[SerializeField] private GModel gameModel;
		[SerializeField] private AudioSource sfxSound;
		[SerializeField] private AudioClip fold, win, unfold;
		private bool loading = true;
		private void Awake()
		{
			backgroundSound.Play();
			/*if (level < PlayerPrefs.GetInt(LayerOrTagName.SaveLevel.ToString()))
			{
				level = PlayerPrefs.GetInt(LayerOrTagName.SaveLevel.ToString());
			}*/

			//LoadLevel();
		}
		private void Update()
		{
			/*if (loadingView.IsLoaded)
			{
				LoadLevel();
				loadingView.IsLoaded = false;
			}*/
			if (loading)
			{
				LoadLevel();
				loading = false;
			}
			level = gameModel.LevelNumber;
		}
		private void LoadLevel()
		{
			Instantiate(levels.LevelPrefabs[gameModel.LevelNumber - 1], parentObject.transform);
/*#if UNITY_STANDALONE || UNITY_WEBGL || UNITY_EDITOR
			//level = 0;
			Instantiate(levels.LevelPrefabs[gameModel.LevelNumber - 1], parentObject.transform);
#else
			if (PlayerPrefs.GetInt(LayerOrTagName.SaveLevel.ToString()) < levels.LevelPrefabs.Count)
			{
				Instantiate(levels.LevelPrefabs[gameModel.LevelNumber - 1], parentObject.transform);
			}
			else
			{
				Logger.Debug("Thanks For Playingggg! See u soon!");
			}
#endif*/
		}
		public int Level { get => level; set => level = value; }
		public Level Levels => levels;
		public GameObject ParentObject => parentObject;

		public AudioSource SFXSound => sfxSound;
		public AudioClip Fold => fold;
		public AudioClip UnFold => unfold;
		public AudioClip Win => win;

	}
}
