using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace PaperFold2D
{
	public class FoldPaper2D : MonoBehaviour
	{
		[Header("FoldPaper")]
		[SerializeField] private GameModel2D gameModel2D;
		[SerializeField] private GameController2D gameController2D;
		[SerializeField] private List<int> rotationOrderIndexes;
		[SerializeField] private List<GameObject> coners;
		[SerializeField] private List<CornerController2D> cornerController2Ds;
		[SerializeField] private float angleChange;
		[SerializeField] private Vector3 rotAxis;
		private int orderIndex = -1;
		private bool rotated = false;
		private bool rightOrder = false;

		[Space(10.0f)]
		[Header("PaperSortingLayer")]
		[SerializeField] private SortingGroup paperSortingGroup;
		[SerializeField] private Renderer mainPictureRenderer;
		[SerializeField] private Renderer backgroundMainPaperRenderer;
		[SerializeField] private Renderer mainPaperBackRenderer;
		[SerializeField] private List<Renderer> subCoverMainPictureRenderers;
		[SerializeField] private List<SortingGroup> subCoverMainPictureSortingGroups;

		[Space(10.0f)]
		[Header("LineCtrl")]
		[SerializeField] private GameObject mainLine;
		[SerializeField] private List<LineRenderer> subLines;
		[SerializeField] private List<int> subLinesIndex;
		[SerializeField] private List<float> subLinesStartPosition;
		[SerializeField] private List<float> subLinesEndPosition;


		public void RotatePaper(bool back)
		{
			foreach (GameObject corner in coners)
			{
				corner.transform.parent = gameObject.transform;
			}

			if (back == false)
			{
				foreach (CornerController2D cornerController2D in cornerController2Ds)
				{
					cornerController2D.CornerRotatedCount++;
				}
				orderIndex = gameModel2D.CurrentOrderIndex;
				gameModel2D.FoldPaper2Ds.Add(this);

				gameModel2D.FoldSound.Play();
				//gameController2D.PlayFoldSound();

				StartCoroutine(FoldAroundAxis2D(gameObject.transform, gameModel2D.FoldPaper2Ds.Count, rotAxis, angleChange,
					new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0), gameModel2D.RotationDuration, back));
				rotated = true;
				foreach (int orderIndex in rotationOrderIndexes)
				{
					if (orderIndex == gameModel2D.CurrentOrderIndex)
					{
						rightOrder = true;
					}
				}
			}
			else
			{
				foreach (CornerController2D cornerController2D in cornerController2Ds)
				{
					cornerController2D.CornerRotatedCount--;
				}
				orderIndex = -1;

				gameModel2D.UnFoldSound.Play();
				//gameController2D.PlayUnFoldSound();

				StartCoroutine(FoldAroundAxis2D(gameObject.transform, gameModel2D.FoldPaper2Ds.Count, rotAxis, -angleChange,
					new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0), gameModel2D.RotationDuration, back));
				gameModel2D.FoldPaper2Ds.Remove(this);
				rotated = false;
				rightOrder = false;
			}
		}

		IEnumerator FoldAroundAxis2D(Transform paperTransform, int orderedIndex, Vector3 rotAxis, float changeInAngle, Vector3 endPosition, float duration, bool back)
		{
			gameModel2D.ClickBlocked = true;
			Quaternion startRotation = paperTransform.rotation;
			Vector3 startPosition = paperTransform.position;

			float t = 0;
			while (t < duration)
			{
				float t2 = gameModel2D.Curve.Evaluate(t / duration);
				paperTransform.rotation = startRotation * Quaternion.AngleAxis(changeInAngle * t2, rotAxis);

				//set line false when Folding
				if (Mathf.Abs(changeInAngle * t / duration) > 40f && Mathf.Abs(changeInAngle * t / duration) < 80f && back == false)
				{
					mainLine.SetActive(false);
					for (int i = 0; i < subLines.Count; i++)
					{
						subLines[i].SetPosition(subLinesIndex[i], new Vector3(0, 0, subLinesEndPosition[i]));
					}
				}

				//set line true when UnFolding
				if (Mathf.Abs(changeInAngle * t / duration) > 150f && Mathf.Abs(changeInAngle * t / duration) < 180f && back == true)
				{
					mainLine.SetActive(true);
					for (int i = 0; i < subLines.Count; i++)
					{
						subLines[i].SetPosition(subLinesIndex[i], new Vector3(0, 0, subLinesStartPosition[i]));
					}
				}

				//set Layer of Paper, Monster, Background, subPaper when fold and unfold.
				if (Mathf.Abs(changeInAngle * t / duration) > 75f && Mathf.Abs(changeInAngle * t / duration) < 105f)
				{
					if (back == false)
					{
						paperSortingGroup.sortingOrder = orderedIndex + 1;
						mainPictureRenderer.sortingLayerName = LayerOrTagName.Fold.ToString();
						mainPictureRenderer.sortingOrder = orderedIndex + 2;
						backgroundMainPaperRenderer.sortingLayerName = LayerOrTagName.Fold.ToString();
						backgroundMainPaperRenderer.sortingOrder = orderedIndex + 1;
						mainPaperBackRenderer.sortingLayerName = LayerOrTagName.Default.ToString();

						foreach (Renderer subCoverMainPaperRenderer in subCoverMainPictureRenderers)
						{
							subCoverMainPaperRenderer.sortingLayerName = LayerOrTagName.Fold.ToString();
							subCoverMainPaperRenderer.sortingOrder = orderedIndex + 3;
						}

						foreach (SortingGroup subCoverMainPaperSortingGroup in subCoverMainPictureSortingGroups)
						{
							subCoverMainPaperSortingGroup.sortingLayerName = LayerOrTagName.Fold.ToString();
							subCoverMainPaperSortingGroup.sortingOrder = 8;
						}

						foreach (CornerController2D cornerController2D in cornerController2Ds)
						{
							cornerController2D.SetLayerCornerFold(LayerOrTagName.Fold.ToString(), LayerOrTagName.Default.ToString(), orderedIndex);
						}
					}
					else
					{
						paperSortingGroup.sortingOrder = 0;
						mainPaperBackRenderer.sortingLayerName = LayerOrTagName.Fold.ToString();
						mainPaperBackRenderer.sortingOrder = 7;
						mainPictureRenderer.sortingLayerName = LayerOrTagName.Default.ToString();
						mainPictureRenderer.sortingOrder = 0;
						backgroundMainPaperRenderer.sortingLayerName = LayerOrTagName.Default.ToString();
						backgroundMainPaperRenderer.sortingOrder = 1;

						foreach (Renderer subCoverMainPaperRenderer in subCoverMainPictureRenderers)
						{
							subCoverMainPaperRenderer.sortingLayerName = LayerOrTagName.Default.ToString();
							subCoverMainPaperRenderer.sortingOrder = 2;
						}

						foreach (SortingGroup subCoverMainPaperSortingGroup in subCoverMainPictureSortingGroups)
						{
							subCoverMainPaperSortingGroup.sortingOrder = 5;
						}

						foreach (CornerController2D cornerController2D in cornerController2Ds)
						{
							cornerController2D.SetLayerCornerUnFold(LayerOrTagName.Fold.ToString(), LayerOrTagName.Default.ToString());
						}
					}
				}
				paperTransform.position = Vector3.Lerp(startPosition, endPosition, t / duration);
				t += Time.deltaTime;
				yield return null;
			}
			paperTransform.rotation = startRotation * Quaternion.AngleAxis(changeInAngle, rotAxis);
			paperTransform.position = endPosition;

			if (back == true)
			{
				gameModel2D.CurrentOrderIndex--;
			}
			else
			{
				gameModel2D.CurrentOrderIndex++;
				if (gameModel2D.MaxNumberOfRotations == gameModel2D.CurrentOrderIndex)
				{
					gameController2D.CheckIfRotatedCorrectly();
				}
			}
			gameModel2D.ClickBlocked = false;
		}
		public int OrderIndex { get => orderIndex; set => orderIndex = value; }
		public bool Rotated { get => rotated; set => rotated = value; }
		public bool RightOrder => rightOrder;
	}
}

