using UnityEngine;
using UnityEngine.EventSystems;

namespace PaperFold2D
{
	public class InputDetector2D : MonoBehaviour, IPointerClickHandler
	{
		[SerializeField] private FoldPaper2D foldPaper2D;
		[SerializeField] private GameModel2D gameModel2D;
		[SerializeField] private GameController2D gameController2D;

		public void OnPointerClick(PointerEventData eventData)
		{
			if (gameController2D.IsInputEnabled == true)
			{
#if UNITY_EDITOR || UNITY_STANDALONE
				Logger.Debug(gameObject.name);
				OnInput();
#else
			if(Input.touchCount > 0 && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
			{
				OnInput();
			}
#endif
			}
		}

		private void OnInput()
		{
			if (gameModel2D.ClickBlocked == false)
			{
				if (foldPaper2D.Rotated == false)
				{
					foldPaper2D.RotatePaper(false);
				}
				else
				{
					if (foldPaper2D.OrderIndex < gameModel2D.CurrentOrderIndex - 1)
					{
						StartCoroutine(gameController2D.RotateBackToIndex(foldPaper2D.OrderIndex));
					}
					else
					{
						foldPaper2D.RotatePaper(true);
					}
				}
			}
		}
	}
}
