using System.Collections.Generic;
using UnityEngine;

namespace PaperFold2D
{
	[CreateAssetMenu(fileName = "Level", menuName = "FoldPaper2D/Level")]
	public class Level : ScriptableObject
	{
		[SerializeField] private List<GameObject> levelPrefabs;

		public List<GameObject> LevelPrefabs => levelPrefabs;
	}
}
