using System.Collections;
using UnityEngine;

namespace PaperFold2D
{
	public class GameController2D : MonoBehaviour
	{
		[SerializeField] private GameModel2D gameModel2D;
		[SerializeField] private GameView2D gameView2D;
		private GameObject levelObj;
		private SaveAndLoadLevel saveAndLoadLevel;
		private bool isInputEnabled = true;
		private bool nextLv = false;

	     private GGamePlayView gamePlay;
		private void Awake()
		{

			levelObj = GameObject.FindGameObjectWithTag(LayerOrTagName.SaveAndLoadLevel.ToString());
			saveAndLoadLevel = levelObj.GetComponent<SaveAndLoadLevel>();
			StartCoroutine(BlockInputDetector(0.5f));
		}
		private void Start()
		{
			gamePlay = GameObject.FindGameObjectWithTag(Constants.GAME_PLAY_TAG_NAME).GetComponent<GGamePlayView>();
		}

		private void Update()
		{
			NextLevel();
		}

		public IEnumerator RotateBackToIndex(int index)
		{
			//BlockInput();
			isInputEnabled = false;
			int tmpIndex = gameModel2D.CurrentOrderIndex;
			if (index == 0 && gameModel2D.FoldPaper2Ds.Count == gameModel2D.MaxNumberOfRotations)
			{
				yield return new WaitForSeconds(1.0f);
			}
			while (gameModel2D.CurrentOrderIndex != index)
			{
				if (tmpIndex == gameModel2D.CurrentOrderIndex)
				{
					gameModel2D.FoldPaper2Ds[tmpIndex - 1].RotatePaper(true);
					tmpIndex--;
				}
				yield return new WaitForEndOfFrame();
			}
			yield return null;
			gameModel2D.Paper.SetBool(VariableNameAnim.isFailed.ToString(), false);
			//UnBlockInput();
			isInputEnabled = true;
		}

		public void CheckIfRotatedCorrectly()
		{
			bool everythingCorrect = true;
			foreach (FoldPaper2D foldPaper in gameModel2D.FoldPaper2Ds)
			{
				if (foldPaper.RightOrder == false)
				{
					everythingCorrect = false;
				}
			}
			// win()
			if (everythingCorrect == true)
			{
				StartCoroutine(gamePlay.Win());

				//StartCoroutine(BlockInputDetector(2f));
				isInputEnabled = false;
				gameModel2D.FinishedBasePicture.sortingLayerName = LayerOrTagName.Picture.ToString();
				//set picture bubble when win
				gameModel2D.Picture.SetActive(true);
				gameModel2D.PictureSortingGroup.sortingLayerName = LayerOrTagName.Picture.ToString();
				gameModel2D.PictureSortingGroup.sortingOrder = 0;

				//win sound On!
				gameModel2D.WinSound.Play();
				//saveAndLoadLevel.SFXSound.PlayOneShot(saveAndLoadLevel.Win);

				//anim paper falling down and bubble picture
				gameModel2D.Paper.SetBool(VariableNameAnim.isWin.ToString(), true);
				gameModel2D.PictureAnimator.SetBool(VariableNameAnim.isWinPicture.ToString(), true);
				Logger.Debug("Animation on, sound on!");
				//saveAndLoadLevel.Level++;
				//PlayerPrefs.SetInt(LayerOrTagName.SaveLevel.ToString(), saveAndLoadLevel.Level);
				//PlayerPrefs.Save();

			}
			else
			{
				//anim paper stagger
				gameModel2D.Paper.SetBool(VariableNameAnim.isFailed.ToString(), true);
				StartCoroutine(RotateBackToIndex(0));
				Logger.Debug("Fail");
			}
		}

		private void DestroyPassedLevl(string passedLvTag)
		{
			GameObject passedLevel = GameObject.FindGameObjectWithTag(passedLvTag);
			if (gameModel2D.PaperObject.transform.position.y < -35.0f)
			{
				Destroy(passedLevel);
				nextLv = true;
			}
		}

		IEnumerator BlockInputDetector(float time)
		{
			//BlockInput();
			isInputEnabled = false;
			yield return new WaitForSeconds(time);
			//UnBlockInput();
			isInputEnabled = true;
		}

		public void NextLevel()
		{
			int currentLevel = saveAndLoadLevel.Level;
			if (currentLevel > 0)
			{
				DestroyPassedLevl(LayerOrTagName.Level.ToString());
			}
			if (currentLevel <= saveAndLoadLevel.Levels.LevelPrefabs.Count && nextLv == true)
			{
				Instantiate(saveAndLoadLevel.Levels.LevelPrefabs[currentLevel-1], saveAndLoadLevel.ParentObject.transform);
			}
			else
			{
				//Logger.Debug("Thanks For Playingggg! See u soon!");
			}
			nextLv = false;
		}
		public bool IsInputEnabled => isInputEnabled;


		// addition
		public void PlayFoldSound()
		{
			saveAndLoadLevel.SFXSound.PlayOneShot(saveAndLoadLevel.Fold);
		}

		public void PlayUnFoldSound()
		{
			saveAndLoadLevel.SFXSound.PlayOneShot(saveAndLoadLevel.UnFold);
		}

}
}
