using UnityEngine;
using UnityEngine.Rendering;

namespace PaperFold2D
{
	public class CornerController2D : MonoBehaviour
	{
		[SerializeField] private int cornerRotatedCount = 0;
		private Renderer cornerRenderer;
		private SortingGroup cornerSortingGroup;

		private void Awake()
		{
			cornerRenderer = gameObject.GetComponent<Renderer>();
			cornerSortingGroup = gameObject.GetComponent<SortingGroup>();
		}

		public void SetLayerCornerFold(string foldLayer, string defaultLayer, int orderedIndex)
		{
			cornerRenderer.sortingLayerName = foldLayer;
			cornerRenderer.sortingOrder = orderedIndex;
			if (cornerRotatedCount == 1)
			{
				cornerSortingGroup.sortingLayerName = foldLayer;
				cornerSortingGroup.sortingOrder = 8;
			}
			else
			{
				cornerSortingGroup.sortingLayerName = defaultLayer;
				cornerSortingGroup.sortingOrder = 8;
			}
		}

		public void SetLayerCornerUnFold(string foldLayer, string defaultLayer)
		{
			if (cornerRotatedCount < 1)
			{
				cornerRenderer.sortingLayerName = defaultLayer;
				cornerRenderer.sortingOrder = 0;
			}
			else
			{
				cornerRenderer.sortingLayerName = foldLayer;
				cornerRenderer.sortingOrder = cornerRotatedCount;
			}
			if (cornerRotatedCount < 2)
			{
				cornerSortingGroup.sortingLayerName = foldLayer;
				cornerSortingGroup.sortingOrder = 8;
			}
		}
		public int CornerRotatedCount { get => cornerRotatedCount; set => cornerRotatedCount = value; }
	}
}
