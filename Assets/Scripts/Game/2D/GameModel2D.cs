using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace PaperFold2D
{
	public class GameModel2D : MonoBehaviour
	{
		[Header("FoldPaper2D")]
		[SerializeField] private float rotationDuration;
		[SerializeField] private AudioSource foldSound;
		[SerializeField] private AudioSource unFoldSound;
		[SerializeField] private AnimationCurve curve;

		[Space(10.0f)]
		[Header("GameController2D")]
		[SerializeField] private int currentOrderIndex = 0;
		[SerializeField] private List<FoldPaper2D> foldPaper2Ds;
		[SerializeField] private int maxNumberOfRotations;
		[SerializeField] private AudioSource winSound;
		private bool clickBlocked = false;

		[Space(10.0f)]
		[Header("Paper")]
		[SerializeField] private GameObject paperObject;
		[SerializeField] private Animator paperAnimator;

		[Space(10.0f)]
		[Header("Picture")]
		[SerializeField] private GameObject picture;
		[SerializeField] private Animator pictureAnimator;
		[SerializeField] private SortingGroup pictureSortingGroup;
		[SerializeField] private Renderer finishedBasePicture;

		private GameObject mainCameraObj;
		private Camera mainCamera;
		private const string mainCameraTag = "MainCamera";

		private void Awake()
		{
			mainCameraObj = GameObject.FindGameObjectWithTag(mainCameraTag);
			mainCamera = mainCameraObj.GetComponent<Camera>();
		}

		public float RotationDuration => rotationDuration;
		public int CurrentOrderIndex { get => currentOrderIndex; set => currentOrderIndex = value; }
		public bool ClickBlocked { get => clickBlocked; set => clickBlocked = value; }
		public List<FoldPaper2D> FoldPaper2Ds { get => foldPaper2Ds; set => foldPaper2Ds = value; }
		public Camera MainCamera => mainCamera;
		public int MaxNumberOfRotations => maxNumberOfRotations;
		public AudioSource FoldSound => foldSound;
		public AudioSource UnFoldSound => unFoldSound;
		public AudioSource WinSound => winSound;
		public Animator Paper => paperAnimator;
		public GameObject PaperObject => paperObject;
		public GameObject Picture => picture;
		public Animator PictureAnimator => pictureAnimator;
		public AnimationCurve Curve => curve;
		public Renderer FinishedBasePicture => finishedBasePicture;
		public SortingGroup PictureSortingGroup => pictureSortingGroup;
	}
}

public enum LayerOrTagName
{
	//LayerName
	Default,
	Fold,
	Picture,
	//Tagname
	SaveAndLoadLevel,
	Level,
	//PlayerPrefs
	SaveLevel,
}

public enum VariableNameAnim
{
	isWin,
	isFailed,
	isWinPicture,
}
