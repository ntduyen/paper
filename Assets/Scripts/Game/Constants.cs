using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PaperFold2D
{
	public static class Constants
	{
		public const string MUSIC_PREF = "music";
		public const string SFX_PREF = "sfx";
		public const string VIBRATION_PREF = "vibration";
		public const string ON_BOOL_PARAM_TOGGLE_ANIMATOR = "on";

		public const string LEVEL_NUMBER_PREF = "levelNumber";
		public const string CURRENT_THEME_INDEX_PREF = "currentThemeIndex";
		public const string GOT_THEME_LIST_PREF = "gotThemeList";

		public const string GAME_PLAY_TAG_NAME = "gameplay";

		public const string MUSIC_PARAM = "music";
		public const string SFX_PARAM = "sfx";
	}
}
