using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoldPaper : MonoBehaviour
{
	[SerializeField] private GameModel gameModelRef;
	[SerializeField] private GameController gameControllerRef;
	[SerializeField] private List<GameObject> coners;
	[SerializeField] private float angleChange;
	[SerializeField] private Vector3 rotAxis;
	private int orderIndex = -1;
	private bool rotated = false;

	[Header("LineCtrl")]
	[SerializeField] private GameObject mainLine;
	[SerializeField] private List<LineRenderer> subLines;
	[SerializeField] private List<int> subLinesIndex;
	[SerializeField] private List<float> subLinesStartPosition;
	[SerializeField] private List<float> subLinesEndPosition;
	[SerializeField] private List<int> rotationOrderIndexes;
	private bool rightOrder = false;

	public void RotatePaper(bool back)
	{
		foreach (GameObject coner in coners)
		{
			coner.transform.parent = gameObject.transform;
		}
		if(back == false)
		{
			orderIndex = gameModelRef.CurrentOrderIndex;
			gameModelRef.FoldPapers.Add(this);
			StartCoroutine(FoldAroundAxis(gameObject.transform, rotAxis, angleChange, transform.position + new Vector3(0, orderIndex * 0.001f + 0.001f, 0), gameModelRef.RotationDuration, back));
			rotated = true;

			mainLine.SetActive(false);
			for(int i = 0; i < subLines.Count; i++)
			{
				subLines[i].SetPosition(subLinesIndex[i], new Vector3(0, 0, subLinesEndPosition[i]));
			}
			foreach(int rotationOrderIndex in rotationOrderIndexes)
			{
				if(rotationOrderIndex == gameModelRef.CurrentOrderIndex)
				{
					rightOrder = true;
				}
			}
		}
		else
		{
			orderIndex = -1;
			StartCoroutine(FoldAroundAxis(gameObject.transform, rotAxis, -angleChange, new Vector3(gameObject.transform.position.x,0, gameObject.transform.position.z), gameModelRef.RotationDuration, back));
			gameModelRef.FoldPapers.Remove(this);
			rotated = false;
			rightOrder = false;
		}
	}

	IEnumerator FoldAroundAxis(Transform paperTransform, Vector3 rotAxis, float changeInAngle, Vector3 endPosition, float duration, bool back)
	{
		gameModelRef.ClickBlocked = true;
		Quaternion startRotation = paperTransform.rotation;
		Vector3 startPosition = paperTransform.position;
		float t = 0f;

		while(t < duration)
		{
			paperTransform.rotation = startRotation * Quaternion.AngleAxis(changeInAngle * gameModelRef.RotationCurve.Evaluate(t/duration), rotAxis);
			paperTransform.position = Vector3.Lerp(startPosition, endPosition, t/duration);
			t += Time.deltaTime;
			yield return null;
		}
		paperTransform.rotation = startRotation * Quaternion.AngleAxis(changeInAngle, rotAxis);
		paperTransform.position = endPosition;

		if( back == true)
		{
			gameModelRef.CurrentOrderIndex--;
			mainLine.SetActive(true);
			for (int i = 0; i < subLines.Count; i++)
			{
				subLines[i].SetPosition(subLinesIndex[i], new Vector3(0, 0, subLinesStartPosition[i]));
			}
		}
		else
		{
			gameModelRef.CurrentOrderIndex++;
			if(gameModelRef.MaxNumberOfRotations == gameModelRef.CurrentOrderIndex )
			{
				gameControllerRef.CheckIfRotatedCorrectly();
			}
		}
		gameModelRef.ClickBlocked = false;
	}
	public bool Rotated => rotated;
	public int OrderIndex => orderIndex;
	public bool RightOrder => rightOrder;
}
