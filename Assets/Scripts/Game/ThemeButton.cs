using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PaperFold2D
{
	public class ThemeButton : MonoBehaviour
	{
		[SerializeField] private Image buttonIcon;
		[SerializeField] private GameObject selectedIcon;

		[SerializeField] private Animator themeButtonAnimator;
		public enum ThemeStatus
		{
			Locked, Unlocked, Used, Got
		}
		private ThemeStatus status;

		public ThemeStatus Status { get => status; set => status = value; }

		public void SetImage(Sprite sprite)
		{
			buttonIcon.sprite = sprite;
		}
		public void SetSelected(bool selected)
		{
			selectedIcon.SetActive(selected);
		}
		public void SetUnlocked()
		{
			buttonIcon.color = new Color(255f, 255f, 255f, 0.5f);
		}
		private void Update()
		{
			switch (status)
			{
				case ThemeStatus.Used:
					SetSelected(true);
					themeButtonAnimator.SetTrigger("Unlocked");
					break;
				case ThemeStatus.Unlocked:
					SetUnlocked();
					SetSelected(false);
					themeButtonAnimator.SetTrigger("Locked");
					break;
				case ThemeStatus.Got:
					SetSelected(false);
					themeButtonAnimator.SetTrigger("Unlocked");
					break;
				case ThemeStatus.Locked:
					SetSelected(false);
					themeButtonAnimator.SetTrigger("Locked");
					break;
			}
		}
	}
}

