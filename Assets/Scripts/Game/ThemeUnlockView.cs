using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PaperFold2D
{
	public class ThemeUnlockView : MonoBehaviour
	{
		[SerializeField] private GameObject declineOfferBtn;
		[SerializeField] private GameObject gamePlayPanel;
		[SerializeField] private GController gameController;
		[SerializeField] private Image themeUnlock;

		private int themeIndex = 0;
		private void Start()
		{
			StartCoroutine(DisplayDeclineButton(3f));
		}
		IEnumerator DisplayDeclineButton(float time)
		{
			yield return new WaitForSeconds(time);
			declineOfferBtn.SetActive(true);
		}

		public void DeclineSkin()
		{
			gameObject.SetActive(false);
			gameController.ButtonList[themeIndex].Status = ThemeButton.ThemeStatus.Unlocked;
			gameController.SetThemeButtonImage(themeIndex);
		}
		public void AcceptSkin()
		{
			gameObject.SetActive(false);
			gameController.SetThemeGamePlayBackground(themeIndex);
			gameController.SetThemeButtonImage(themeIndex);
			gameController.ButtonList[themeIndex].Status = ThemeButton.ThemeStatus.Used;
			gameController.AddToGotThemeList(themeIndex);
		}
		public void SetThemeUnlock(Sprite sprite)
		{
			themeUnlock.sprite = sprite;
		}
		public int ThemeIndex { set => themeIndex = value; }
	}
}
