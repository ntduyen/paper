using UnityEngine;

public class InputDetector : MonoBehaviour
{
	[SerializeField] FoldPaper foldPaperRef;

	public FoldPaper FoldPaper => foldPaperRef;
}
