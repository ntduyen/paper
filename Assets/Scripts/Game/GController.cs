using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace PaperFold2D
{
	public class GController : MonoBehaviour
	{
		[SerializeField] private GModel gameModel;
		[SerializeField] private GGamePlayView gamePlayView;
		[SerializeField] private ThemeUnlockView unlockThemeView;
		[SerializeField] private ThemePopupView themePopup;

		private Sprite[] themeList;
		private Sprite[] themeIconList;
		private Sprite[] backgroundList;

		[SerializeField] private ThemeButton[] buttonList;

		private int themeUnlockedNumber;
		private int numberLevelToNewTheme = 5;

		private List<int> gotThemeList;
		private string gotThemeListString;
		private void Start()
		{
			gotThemeList = new List<int>();
			gotThemeListString = "0";

			// reset data

			PlayerPrefs.DeleteKey(Constants.GOT_THEME_LIST_PREF);
			PlayerPrefs.SetInt(Constants.LEVEL_NUMBER_PREF, 1);
			PlayerPrefs.SetInt(Constants.CURRENT_THEME_INDEX_PREF, 0);

			if (PlayerPrefs.HasKey(Constants.GOT_THEME_LIST_PREF))
			{
				gotThemeListString = PlayerPrefs.GetString(Constants.GOT_THEME_LIST_PREF);
				if (gotThemeListString != "")
				{
					string[] array = gotThemeListString.Split(',');
					foreach (string number in array)
					{
						gotThemeList.Add(Int32.Parse(number));
					}
				}
			}
			// get data from model
			themeList = gameModel.ThemeList;
			themeIconList = gameModel.ThemeIconList;
			backgroundList = gameModel.BackgroundList;

			// load current background, levelNumber
			if (!PlayerPrefs.HasKey(Constants.CURRENT_THEME_INDEX_PREF))
			{
				gameModel.CurrentBackgroundIndex = 0;
			}
			gameModel.CurrentBackgroundIndex = PlayerPrefs.GetInt(Constants.CURRENT_THEME_INDEX_PREF);

			if (!PlayerPrefs.HasKey(Constants.LEVEL_NUMBER_PREF))
			{
				gameModel.LevelNumber = 1;
			}
			gameModel.LevelNumber = PlayerPrefs.GetInt(Constants.LEVEL_NUMBER_PREF);

			// get data of barprogress
			gamePlayView.ResetProgressBar();
			for (int i = 0; i <= (gameModel.LevelNumber - 1) % numberLevelToNewTheme; i++)
			{
				gamePlayView.SetOn(i);
			}
			// get the number of unlocked themes
			themeUnlockedNumber = (gameModel.LevelNumber - 1) / numberLevelToNewTheme;
			// set background, set theme icon in progressbar
			SetThemeGamePlayBackground(gameModel.CurrentBackgroundIndex);
			SetIconSkinInProgressBar();
			// set themebutton data

			for (int i = 0; i < buttonList.Length; i++)
			{
				buttonList[i].Status = ThemeButton.ThemeStatus.Locked;
				if (i <= themeUnlockedNumber)
				{
					SetThemeButtonImage(i);
					buttonList[i].Status = ThemeButton.ThemeStatus.Unlocked;
				}
				if (gotThemeList.Contains(i))
				{
					buttonList[i].Status = ThemeButton.ThemeStatus.Got;
				}
				if (i == gameModel.CurrentBackgroundIndex)
				{
					buttonList[i].Status = ThemeButton.ThemeStatus.Used;
				}
			}

		}

		// Update is called once per frame
		void Update()
		{
			// Display level number
			gamePlayView.DisplayLevelNumberText(gameModel.LevelNumber);
		}
		public void Win()
		{
			gamePlayView.SetOn((gameModel.LevelNumber) % 5);
			if (gameModel.LevelNumber % numberLevelToNewTheme == 0)
			{
				UnlockTheme();
				gamePlayView.ResetProgressBar();
			}
			gameModel.LevelNumber++;
		}
		public void SetThemeGamePlayBackground(int index)
		{
			buttonList[gameModel.CurrentBackgroundIndex].Status = ThemeButton.ThemeStatus.Got;
			gameModel.CurrentBackgroundIndex = index;
			gamePlayView.SetBackGround(backgroundList[index]);
		}
		public void UnlockTheme()
		{
			unlockThemeView.gameObject.SetActive(true);
			themeUnlockedNumber++;

			unlockThemeView.SetThemeUnlock(themeList[themeUnlockedNumber]);
			unlockThemeView.ThemeIndex = themeUnlockedNumber;
			unlockThemeView.gameObject.SetActive(true);
			SetIconSkinInProgressBar();
		}
		public void SetIconSkinInProgressBar()
		{
			gamePlayView.SetIconSkinProgressBar(themeIconList[themeUnlockedNumber], themeIconList[themeUnlockedNumber + 1]);
		}

		public void SetThemeButtonImage(int index)
		{
			buttonList[index].SetImage(themeIconList[index]);
		}

		public void AddToGotThemeList(int i)
		{
			gotThemeListString += "," + i;
		}
		public ThemeButton[] ButtonList => buttonList;

		private void OnApplicationQuit()
		{
			PlayerPrefs.SetInt(Constants.LEVEL_NUMBER_PREF, gameModel.LevelNumber);
			PlayerPrefs.SetInt(Constants.CURRENT_THEME_INDEX_PREF, gameModel.CurrentBackgroundIndex);
			PlayerPrefs.SetString(Constants.GOT_THEME_LIST_PREF, gotThemeListString);
			Logger.Debug(gotThemeListString);
		}
	}
}

