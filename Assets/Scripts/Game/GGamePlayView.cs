using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace PaperFold2D
{
	public class GGamePlayView : MonoBehaviour
	{
		[SerializeField] private GameObject winPanel;

		[SerializeField] private TMP_Text levelNumberText;

		[SerializeField] private GameObject themeOfferPopup;

		[SerializeField] private GController gameController;

		[SerializeField] private Image backGround;

		[SerializeField] private Image currentSkin;
		[SerializeField] private Image nextSkin;

		[SerializeField] private Sprite on;
		[SerializeField] private Sprite off;

		[SerializeField] private Image[] levels;
		public void DisplayLevelNumberText(int number)
		{
			levelNumberText.text = $"LEVEL {number}";
		}
		public void SetBackGround(Sprite sprite)
		{
			backGround.sprite = sprite;
		}
		/*public void OnButtonClick()
		{
			StartCoroutine(Win());
		}
*/
		public IEnumerator Win()
		{
			winPanel.SetActive(true);
			yield return new WaitForSeconds(1.5f);
			winPanel.SetActive(false);
			gameController.Win();
		}


		public void SetIconSkinProgressBar(Sprite currentIcon, Sprite nextIcon)
		{
			currentSkin.sprite = currentIcon;
			nextSkin.sprite = nextIcon;
		}
		public void ResetProgressBar()
		{
			foreach (Image level in levels)
			{
				level.sprite = off;
			}
			SetOn(0);
		}

		public void SetOn(int index)
		{
			levels[index].sprite = on;
		}
	}
}

