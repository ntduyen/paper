using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace PaperFold2D
{
	public class GameLoadingView : MonoBehaviour
	{
		[SerializeField] private GameObject loadingGamePanel;
		[SerializeField] private GameObject gamePlayPanel;
		[SerializeField] private Image loadingBarFill;
		private bool isLoading = true;
		private bool isLoaded = false;
		private void Update()
		{
			if (isLoading)
			{
				LoadGame();
			}
		}
		public void LoadGame()
		{
			if (loadingBarFill.fillAmount < 0.9f)
			{
				loadingBarFill.fillAmount += Time.deltaTime * Random.Range(0f, 1f);
				//Logger.Debug(loadingBarFill.fillAmount);
			}
			else
			{
				isLoading = false;
				StartCoroutine(Load(Random.Range(2f, 5f)));
			}
		}
		IEnumerator Load(float time)
		{
			yield return new WaitForSeconds(time);
			loadingGamePanel.SetActive(false);
			gamePlayPanel.SetActive(true);
			isLoaded = true;
		}
		public bool IsLoaded { get => isLoaded; set => isLoaded = value; }
	}


}
