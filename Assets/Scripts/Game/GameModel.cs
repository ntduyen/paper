using System.Collections.Generic;
using UnityEngine;

public class GameModel : MonoBehaviour
{
	[Header("FoldPaper")]
	[SerializeField] private float rotationDuration;
	[SerializeField] private AudioSource foldPaperSound;
	[SerializeField] private AudioSource unFoldPaperSound;

	[Header("Controller")]
	[SerializeField] private int currentOrderIndex = 0;
	[SerializeField] private List<FoldPaper> foldPapers;
	[SerializeField] private AnimationCurve rotationCurve;
	[SerializeField] private int maxNumberOfRotations;
	[SerializeField] private AudioSource winSound;
	[SerializeField] private AudioSource backgroundSound;
	private bool clickBlocked = false;

	public float RotationDuration => rotationDuration;
	public int CurrentOrderIndex { get => currentOrderIndex; set => currentOrderIndex = value; }
	public bool ClickBlocked { get => clickBlocked; set => clickBlocked = value; }
	public List<FoldPaper> FoldPapers { get => foldPapers; set => foldPapers = value; }
	public AnimationCurve RotationCurve => rotationCurve;
	public int MaxNumberOfRotations => maxNumberOfRotations;
	public AudioSource FoldPaperSound => foldPaperSound;
	public AudioSource UnFoldPaperSound => unFoldPaperSound;
	public AudioSource WinSound => winSound;
	public AudioSource BackgroundSound => backgroundSound;
}
