using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PaperFold2D
{
	public class GModel : MonoBehaviour
	{
		[SerializeField] private Sprite[] themeList;
		[SerializeField] private Sprite[] themeIconList;
		[SerializeField] private Sprite[] backgroundList;
		private int levelNumber;
		private int currentBackgroundIndex;

		public Sprite[] ThemeList => themeList;
		public Sprite[] ThemeIconList => themeIconList;
		public Sprite[] BackgroundList => backgroundList;
		public int LevelNumber { get => levelNumber; set => levelNumber = value; }
		public int CurrentBackgroundIndex { get => currentBackgroundIndex; set => currentBackgroundIndex = value; }
	}
}

