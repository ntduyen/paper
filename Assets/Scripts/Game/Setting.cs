using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

namespace PaperFold2D
{
	public class Setting : MonoBehaviour
	{
		[SerializeField] private Toggle vibrationToggle;
		[SerializeField] private Toggle sfxToggle;
		[SerializeField] private Toggle musicToggle;

		[SerializeField] private AudioSource musicSource;
		[SerializeField] private AudioSource sfxSource;

		[SerializeField] private Animator musicAnimator;
		[SerializeField] private Animator sfxAnimator;
		[SerializeField] private Animator vibrationAnimator;


		[SerializeField] private AudioMixer myMixer;

		// Start is called before the first frame update
		private void Start()
		{
			if (!PlayerPrefs.HasKey(Constants.VIBRATION_PREF))
			{
				vibrationToggle.isOn = true;
			}
			if (!PlayerPrefs.HasKey(Constants.MUSIC_PREF))
			{
				sfxToggle.isOn = true;
			}
			if (!PlayerPrefs.HasKey(Constants.SFX_PREF))
			{
				musicToggle.isOn = true;
			}
			vibrationToggle.isOn = PlayerPrefs.GetInt(Constants.VIBRATION_PREF) == 1;
			sfxToggle.isOn = PlayerPrefs.GetInt(Constants.SFX_PREF) == 1;
			musicToggle.isOn = PlayerPrefs.GetInt(Constants.MUSIC_PREF) == 1;

			vibrationToggle.onValueChanged.AddListener(OnSwitchVibration);
			musicToggle.onValueChanged.AddListener(OnSwitchMusic);
			sfxToggle.onValueChanged.AddListener(OnSwitchSFX);

			//musicSource.mute = !musicToggle.isOn;
			//sfxSource.mute = !sfxToggle.isOn;
			Logger.Debug(musicToggle.isOn);
			myMixer.SetFloat(Constants.MUSIC_PARAM, Mathf.Log10(musicToggle.isOn ? 10f : 0.0001f) * 20);
			myMixer.SetFloat(Constants.SFX_PARAM, Mathf.Log10(sfxToggle.isOn ? 10f : 0.0001f) * 20);

			musicAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !musicToggle.isOn);
			sfxAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !sfxToggle.isOn);
			vibrationAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !vibrationToggle.isOn);
		}
		public void OnSwitchMusic(bool on)
		{
			//musicSource.mute = !on;
			myMixer.SetFloat(Constants.MUSIC_PARAM, Mathf.Log10(musicToggle.isOn ? 10f : 0.0001f) * 20);

			musicAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !on);
			//Logger.Debug(musicToggle.isOn);
		}
		public void OnSwitchSFX(bool on)
		{
			//sfxSource.mute = !on;
			myMixer.SetFloat(Constants.SFX_PARAM, Mathf.Log10(sfxToggle.isOn ? 10f : 0.0001f) * 20);

			sfxAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !on);
		}
		public void OnSwitchVibration(bool on)
		{
			//musicSource.mute = on;
			vibrationAnimator.SetBool(Constants.ON_BOOL_PARAM_TOGGLE_ANIMATOR, !on);
		}
		public void CloseSettingPanel()
		{
			PlayerPrefs.SetInt(Constants.MUSIC_PREF, musicToggle.isOn ? 1 : 0);
			PlayerPrefs.SetInt(Constants.SFX_PREF, sfxToggle.isOn ? 1 : 0);
			PlayerPrefs.SetInt(Constants.VIBRATION_PREF, vibrationToggle.isOn ? 1 : 0);
		}
		private void OnApplicationQuit()
		{
			CloseSettingPanel();
		}
	}
}

