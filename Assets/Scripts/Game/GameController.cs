using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField] private GameModel gameModelRef;
	[SerializeField] private GameView gameViewRef;
	private GameObject objPaper;
	private Rigidbody paperRigibody;
	private const string paperTag = "Paper";

	private void Start()
	{
		objPaper = GameObject.FindGameObjectWithTag(paperTag);
		paperRigibody = objPaper.GetComponent<Rigidbody>();
		paperRigibody.constraints = RigidbodyConstraints.FreezePositionY;
	}

	private void Update()
	{
#if UNITY_ANDROID
			if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
			{
				OnInput(Input.touches[0].position);
			}
#endif

#if UNITY_STANDALONE || UNITY_WEBGL || UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			OnInput(Input.mousePosition);
			Logger.Debug("ClickBlocked: " + gameModelRef.ClickBlocked);
			Logger.Debug("Mouse down");
		}
#endif
	}

	private void OnInput(Vector3 inputPos)
	{
		Ray ray = Camera.main.ScreenPointToRay(inputPos);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 100))
		{
			if (gameModelRef.ClickBlocked == false)
			{
				if (hit.transform.GetComponent<InputDetector>().FoldPaper.Rotated == false)
				{
					hit.transform.GetComponent<InputDetector>().FoldPaper.RotatePaper(false);
					gameModelRef.FoldPaperSound.Play();
				}
				else
				{
					if (hit.transform.GetComponent<InputDetector>().FoldPaper.OrderIndex < gameModelRef.CurrentOrderIndex - 1)
					{
						StartCoroutine(RotateBackToIndex(hit.transform.GetComponent<InputDetector>().FoldPaper.OrderIndex));
						gameModelRef.UnFoldPaperSound.Play();
					}
					else
					{
						hit.transform.GetComponent<InputDetector>().FoldPaper.RotatePaper(true);
						gameModelRef.UnFoldPaperSound.Play();
					}
				}
			}
		}
	}

	IEnumerator RotateBackToIndex(int index)
	{
		int tmpIndex = gameModelRef.CurrentOrderIndex;
		while (gameModelRef.CurrentOrderIndex != index)
		{
			if (tmpIndex == gameModelRef.CurrentOrderIndex)
			{
				gameModelRef.FoldPapers[tmpIndex - 1].RotatePaper(true);
				tmpIndex--;
			}
			yield return new WaitForEndOfFrame();
		}
		yield return null;
	}

	public void CheckIfRotatedCorrectly()
	{
		bool everythingCorrect = true;
		foreach (FoldPaper foldPaper in gameModelRef.FoldPapers)
		{
			if (foldPaper.RightOrder == false)
			{
				everythingCorrect = false;
			}
		}
		if (everythingCorrect == true)
		{
			gameModelRef.WinSound.Play();
			Logger.Debug("Winnnnnnnn, Sound On!");
			paperRigibody.AddForce(new Vector3(0, 0, 4), ForceMode.Impulse);
			StartCoroutine(PaperFalling(paperRigibody));
			paperRigibody.AddTorque(new Vector3(0, -3, 0), ForceMode.Force);
		}
		else
		{
			StartCoroutine(RotateBackToIndex(0));
			Logger.Debug("Failed");
		}
	}

	IEnumerator PaperFalling(Rigidbody rb)
	{
		yield return new WaitForSeconds(0.25f);
		rb.AddForce(new Vector3(0, 0, -8), ForceMode.Impulse);
	}
}
