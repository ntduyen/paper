using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PaperFold2D
{
	public class ThemePopupView : MonoBehaviour
	{
		[SerializeField] private GController gameController;

		public void OnThemeButtonClick(int index)
		{
			if (gameController.ButtonList[index].Status != ThemeButton.ThemeStatus.Unlocked &&
				gameController.ButtonList[index].Status != ThemeButton.ThemeStatus.Locked)
			{
				gameController.SetThemeGamePlayBackground(index);
				gameController.ButtonList[index].Status = ThemeButton.ThemeStatus.Used;
			}
		}
	}
}
